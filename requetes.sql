-- NOM DU PROJET: B42 Projet 1
-- NOM DU FICHIER: requetes.sql
-- DATE CR�ATION: 2018-09-21
-- DESCRIPTION: Requetes demand�es 
-- AUTEURS: Jean-Charles Bertrand, Yasmine Kaddouri, Lynda Lavoie, Danick Massicotte

PROMPT  
PROMPT =================================================================
PROMPT  Requete : Donner la liste des joueurs : alias, date inscription 
PROMPT =================================================================

SELECT alias "Alias", date_inscription "date d'inscription" FROM joueur;

PROMPT  
PROMPT =============================================================
PROMPT  Requete : Donner la liste d'avatar d'un joueur quelconque :
PROMPT  nom couleur date de cr�ation suivant le format 2000-12-25 
PROMPT =============================================================

SELECT
	a.nom "Nom",
	c.red "Rouge",
	c.green "Vert",
	c.blue "Bleu",
	TO_CHAR(a.date_creation, 'YYYY-MM-DD') "Date de cr�ation"
	FROM avatar a
		JOIN couleur_preferee c
			ON a.id = c.id_avatar
	WHERE id_joueur = (
		SELECT id
		FROM joueur
		WHERE alias LIKE '%*')
	ORDER BY a.nom;
    
PROMPT  
PROMPT =======================================================================================
PROMPT  Requete :  Pour l'avatar principal: donnez toutes les caract�ristiques qu'il poss�de:
PROMPT  nom dates obtention et niveaux 
PROMPT =======================================================================================	

SELECT 
	cm.nom "Nom caract�ristique", 
	ca.date_obtention "Date d'obtention",
	ca.niveau "Niveau"
	FROM carac_monde cm
		JOIN carac_avatar ca
			ON cm.id = ca.id_carac_monde
	WHERE id_avatar = (
		SELECT id
		FROM avatar
		WHERE nom LIKE '%*');
PROMPT  
PROMPT =============================================================================================
PROMPT  Requete :  pour l''avatar principal: donnez la valeur totale de tous les items qu''il poss�de
PROMPT ==============================================================================================

SELECT SUM(ir.cout) "Valeur totale"
	FROM items_rare ir
		JOIN items_obtenus io
			ON ir.id = io.id_items_rare
	WHERE io.id_avatar = (
		SELECT id
		FROM avatar
        WHERE nom LIKE '%*');
PROMPT  
PROMPT ==================================================================================================
PROMPT  Requete :  pour le joueur principal donnez le nombre total d''heures pass�es dans chaque jeu jou� 
PROMPT ==================================================================================================

SELECT (SELECT nom FROM monde where id = am.id_monde), SUM(am.duree)
FROM activite a
    JOIN activite_monde am
    ON a.id = am.id_activite
WHERE a.id_joueur = (
    SELECT id
    FROM joueur
    WHERE alias LIKE '%*')
    GROUP BY am.id_monde;
    
PROMPT  
PROMPT ====================================================================
PROMPT  Requete :  Donnez la liste de chaque joueur et le montant d�pens� 
PROMPT ====================================================================

SELECT (SELECT nom FROM monde where id = am.id_monde)"Monde", SUM(am.duree)"Somme"
FROM ACHAT
GROUP BY id_joueur;

PROMPT  
PROMPT ==========================================================================================================
PROMPT  Requete : donnez la liste de tous les avatars qui poss�de plus d''un item : nom de l''avatar et du joueur  
PROMPT ==========================================================================================================

SELECT
  a.nom "Avatar",
  j.alias "Joueur"
  FROM avatar a
    JOIN joueur j
      ON j.id = a.id_joueur
  WHERE a.id IN (
    SELECT id_avatar
    FROM items_obtenus
    GROUP BY id_avatar
    HAVING SUM(quantite) > 1)
  ORDER BY a.nom;


PROMPT  
PROMPT =======================================================================================================================================
PROMPT Requ�te personnalis�e 1 - affiche les items obtenus, le monde d''o� ils proviennent et le nombre de fois qu''ils ont �t� achet�s/obtenus
PROMPT =======================================================================================================================================   
-- jonction de deux tables:
SELECT ( SELECT nom FROM items_rare  WHERE id= io.id_items_rare)"Items" ,(SELECT nom FROM monde WHERE id=id_monde)"Monde", SUM(io.quantite) as somme
  FROM items_obtenus io
  JOIN items_rare ir
  ON  io.id_items_rare = ir.id
GROUP BY io.id_items_rare, ir.id_monde, io.quantite;


PROMPT  
PROMPT =========================================================================================
PROMPT Requ�te personnalis�e 2 - Affiche la liste des mondes visit�s par un avatar et les dates
PROMPT =========================================================================================   
-- jonction de trois tables:
SELECT ( SELECT nom FROM avatar  WHERE id= id_avatar)"Nom", (SELECT nom FROM monde WHERE id=id_monde)"Monde", a.date_heure_debut
FROM activite a
JOIN activite_monde am
ON  am.id_activite = a.id
JOIN activite_avatar aa
ON aa.id_activite = a.id;



PROMPT  
PROMPT =========================================================================================
PROMPT Requ�te personnalis�e - Liste des items rares achet�s par le joueur JiciTroy450 
PROMPT par mode paiement paypal et qui appartiennent au monde AVACYN
PROMPT =========================================================================================   
-- jonction de quatre tables:       
SELECT  ir.nom "Items rares"
    FROM achat a
        JOIN items_rare ir
        ON ir.id = a.id_item_rare
        WHERE ir.id_monde = (SELECT id FROM monde WHERE sigle = 'AVACYN')
        AND
        a.id_mode_paiement = (SELECT id FROM mode_paiement WHERE nom = 'paypal')
        AND 
        a.id_joueur = (SELECT id FROM joueur WHERE alias = 'JiciTroy450');
        


    

    
    
    
