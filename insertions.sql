-- NOM DU PROJET: B42 Projet 1
-- NOM DU FICHIER: insertions.sql
-- DATE CR�ATION: 2018-09-21
-- DESCRIPTION: Insertion dans les tables
-- AUTEURS: Jean-Charles Bertrand, Yasmine Kaddouri, Lynda Lavoie, Danick Massicotte

--++++++++++++++++++
-- note pour le professeur: 
-- pour les insertions des joueurs, la date_inscription serait logiquement 
-- SYSDATE mais nous avons "hardcod�" le 2 oct 2018 pour �viter les conflits durant les tests
--++++++++++++++++++

PROMPT  
PROMPT =======================================================
PROMPT Insertions modes de paiement
PROMPT =======================================================

INSERT INTO mode_paiement(nom) 
    VALUES('carte de cr�dit');

INSERT INTO mode_paiement(nom) 
    VALUES('paypal');

INSERT INTO mode_paiement(nom) 
    VALUES('interact');

PROMPT  
PROMPT =======================================================
PROMPT Insertions types achats
PROMPT =======================================================

INSERT INTO type_achat(nom) 
    VALUES('temps de jeu');

INSERT INTO type_achat(nom) 
    VALUES('item rare');

PROMPT  
PROMPT =======================================================
PROMPT Insertion des mondes
PROMPT =======================================================

INSERT INTO monde(nom, sigle, description) 
    VALUES('Avacyn', 'AVACYN', 'Univers Gothique / Horreur');

INSERT INTO monde (nom, sigle, description)
    VALUES ('H�ropolis', 'HROPLS', 'Utiliser vos habilet�s surhumaines et venez
        en aide au monde! Que ce soit contre une invasion extra-terrestre ou contre la dixi�me
        tentative de domination du monde d''un super-criminel, joignez vos forces avec celles 
        d''alli�s et prot�gez les civils contre les diverses menaces! Qui sait: peut-�tre 
        monterez-vous au rang de l�gende et ferez-vous partie de la Ligue des Justiciers
        L�gendaires?');
        
INSERT INTO monde (nom, sigle, description)
    VALUES ('Salonlitt�raire', 'LEIWIS', 'Lieu de mondanit�s r�unissant grands esprits,
    amateurs de beaux-Arts et cosmopolites f�rus de conversation et de bonne chair,
    cet espace est ouvert aux modernes et intellectuels des deux sexes et de toute condition, 
    faisant fi des conventions sociales et du bon ton de l��poque. Dans cet univers du bon go�t, 
    on pourra faire �voluer nos avatars, qui devront troquer leurs combinaisons spatiales 
    ou leurs capes h�ro�ques pour chapeaux � plumes, cannes � pommeaux d�or et gants de velours. ');
	
INSERT INTO monde (nom, sigle, description)
	VALUES ('Endaerya', 'ENDRYA', 
	'L''arm�e des n�cromanciens a envahi le paisible monde d''Endaerya. Vous �tes l''�lu de la lumi�re, 
	choisi par les pr�tres-magiciens de Mayka.
	Saurez-vous porter la flamme de la d�esse pour vaincre les cr�atures d�moniaques et lib�rer Endearya?');
        
        
PROMPT  
PROMPT =======================================================
PROMPT Insertions des caract�ristiques rares pour les mondes
PROMPT =======================================================

INSERT INTO carac_monde(nom, sigle, energie_acquisition, energie_utilisation, description, id_monde) 
    VALUES ('Superforce', 'FHF', 100.00, 350.00, 'Permet au h�ro de d�placer des objets lourds, de 
	frapper au travers d''un mur. Augmente la Force totale du personnage.',(SELECT id FROM monde WHERE sigle = 'HROPLS'));

INSERT INTO carac_monde(nom, sigle, energie_acquisition, energie_utilisation, description, id_monde) 
    VALUES ('T�l�kin�sie', 'FHT', 250.00, 500.00, 'Donne le pouvoir de d�placer des objets � distance, 
        ainsi que d''attaquer des adversaires ou les immobiliser avec le pouvoir de la pens�e. Augmente l''Int�lligence totale du
        personnage.',(SELECT id FROM monde WHERE sigle = 'HROPLS'));
        
INSERT INTO carac_monde(nom, sigle, energie_acquisition, energie_utilisation, description, id_monde) 
    VALUES ('Invisibilit�', 'FVI', 150.00, 800.00, 'Invisibilit� temporaire',(SELECT id FROM monde WHERE sigle = 'AVACYN'));
    
INSERT INTO carac_monde(nom, sigle, energie_acquisition, energie_utilisation, description, id_monde) 
    VALUES ('Invincibilit�', 'FIN', 250.00, 1000.00, 'Invincibilit� temporaire',(SELECT id FROM monde WHERE sigle = 'AVACYN'));

INSERT INTO carac_monde(nom, sigle, energie_acquisition, energie_utilisation, description, id_monde) 
    VALUES ('Irr�v�rence', 'FLI', 250.00, 900.00, 'Pr�tention infinie',(SELECT id FROM monde WHERE sigle = 'LEIWIS'));

INSERT INTO carac_monde(nom, sigle, energie_acquisition, energie_utilisation, description, id_monde) 
    VALUES ('Connaissance', 'FLC', 150.00, 1000.00, 'Connaissance infinie',(SELECT id FROM monde WHERE sigle = 'LEIWIS'));
INSERT INTO carac_monde (nom, sigle, energie_acquisition, energie_utilisation, description, id_monde)
	VALUES ('Gu�rison Miraculeuse', 'FEG', 50.00, 350.00, 'Gu�rison des blessures, annule les effets n�gatifs et poisons',(SELECT id FROM monde WHERE sigle = 'ENDRYA'));

INSERT INTO carac_monde (nom, sigle, energie_acquisition, energie_utilisation, description, id_monde)
	VALUES ('Lumi�re Sacr�e de Mayka', 'FEL', 200.00, 500.00, 'Inonde le monde de lumi�re, aveugle et br�le les ennemis mal�fiques',(SELECT id FROM monde WHERE sigle = 'ENDRYA'));	

PROMPT  
PROMPT =======================================================
PROMPT Insertions des items rares pour les mondes
PROMPT =======================================================

-- Items pour H�ropolis
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
    VALUES ((SELECT id FROM monde WHERE sigle = 'HROPLS'),'Jetpack', 'IHJP', 0.15098921, 15000.00, 'Tout le monde veut voler; maintenant vous pouvez!');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
    VALUES ((SELECT id FROM monde WHERE sigle = 'HROPLS'),'Megalixir', 'IHMG', 0.00194892, 2000.00, 'Devient g�ant; + Force, + Constitution, + Taille');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description) 
    VALUES ((SELECT id FROM monde WHERE sigle = 'HROPLS'),'Bouclier NanoTech FS425', 'IHFS', 0.11544786, 45000.00, 'Bouclier dernier cri de NanoTech; + D�fense');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description) 
    VALUES ((SELECT id FROM monde WHERE sigle = 'HROPLS'),'Minilixir', 'IHMN', 0.21514669, 2000.00, 'Devient mini; + Agilit�, + �vasion, - Taille');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description) 
    VALUES ((SELECT id FROM monde WHERE sigle = 'HROPLS'),'R�acteur Quantique QRX-9', 'IHQR', 0.12501514, 50000.00, 'Objet rare pour augmenter votre costume');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description) 
    VALUES ((SELECT id FROM monde WHERE sigle = 'HROPLS'),'Gants ''Shockwave''', 'IHSG', 0.15698847, 37500.00, 'Gants inspir�s d''un villain de type C, mais largement am�lior�es; + D�fense');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description) 
    VALUES ((SELECT id FROM monde WHERE sigle = 'HROPLS'),'Fusil PhotonBlast', 'IHPB', 0.02012248, 250000.00, 'Prototype fonctionnel de l''arme militaire discontinu� PhotonBlast; + Attaque');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description) 
    VALUES ((SELECT id FROM monde WHERE sigle = 'HROPLS'),'Ceinture NanoTech UB7126', 'IHUB', 0.07513254, 50000.00, 'Plus de pochettes! + D�fense, + Place Inventaire');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description) 
    VALUES ((SELECT id FROM monde WHERE sigle = 'HROPLS'),'Bottes ''Shockwave''', 'IHSB', 0.15488758, 40000.00, 'Accompagnent les gants du m�me nom; + D�fense');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description) 
    VALUES ((SELECT id FROM monde WHERE sigle = 'HROPLS'),'Totem: Mars', 'IHMA', 0.01111225, 150000.00, 'Effigie du dieu Mars. Augmente dramatiquement votre Force. Permanent');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description) 
    VALUES ((SELECT id FROM monde WHERE sigle = 'HROPLS'),'Totem: Minerva', 'IHMI', 0.50109489, 40000.00, 'Effigie de la d�esse Minerva. Augmente dramatiquement votre Intelligence. Permanent');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description) 
    VALUES ((SELECT id FROM monde WHERE sigle = 'HROPLS'),'Totem: Mercure', 'IHME', 0.00116489, 100000.00, 'Effigie du dieu Mercure. Augmente dramatiquement votre vitesse. Permanent');
    
-- Items pour Avacyn    
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
    VALUES ((SELECT id FROM monde WHERE sigle = 'AVACYN'),'Crucifix', 'IACX', 0.09546998, 13000.00, 'Augmente les dommages contre les mort-vivants');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
    VALUES ((SELECT id FROM monde WHERE sigle = 'AVACYN'),'Balle en argent', 'IABA', 0.14104702, 2000.00, 'Dommage accru contre les Lycans');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
    VALUES ((SELECT id FROM monde WHERE sigle = 'AVACYN'),'Papyrus', 'IAPY', 0.08466321, 5000.00, 'Augmente les statistiques de d�veloppement');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
    VALUES ((SELECT id FROM monde WHERE sigle = 'AVACYN'),'Anneau', 'IANO', 0.04497663, 45000.00, 'Augmente d�fense');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
    VALUES ((SELECT id FROM monde WHERE sigle = 'AVACYN'),'Torche', 'IATO', 0.03487962, 1000.00, 'Meilleure visibilit� de la map de jeu');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
    VALUES ((SELECT id FROM monde WHERE sigle = 'AVACYN'),'Pieux', 'IAPI', 0.11477963, 7500.00, 'Augmente les chances de tuer un mort-vivant d''un seul coup');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
    VALUES ((SELECT id FROM monde WHERE sigle = 'AVACYN'),'Eau b�nite', 'IAEB', 0.17144047, 2500.00, 'Augmente dommages contre les vampires');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
    VALUES ((SELECT id FROM monde WHERE sigle = 'AVACYN'),'Chapelet', 'IACP', 0.01256489, 7000.00, 'Protection accrue contre vampire et Lycans');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
    VALUES ((SELECT id FROM monde WHERE sigle = 'AVACYN'),'Bible', 'IABL', 0.02501136, 12000.00, 'Augmentation niveau de connaissances');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
    VALUES ((SELECT id FROM monde WHERE sigle = 'AVACYN'),'Arbal�te', 'IARB', 0.13487966, 32000.00, 'Arme longue port�e rechargeable');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
    VALUES ((SELECT id FROM monde WHERE sigle = 'AVACYN'),'Armure en cuir de Lycan', 'IAAL', 0.04144847, 78000.00, 'Vitesse de d�placement augment�e');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
    VALUES ((SELECT id FROM monde WHERE sigle = 'AVACYN'),'Trident', 'IATR', 0.11025465, 55000.00, 'Arme deux mains longue port�e');
    
-- Items pour Leiwis 
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
    VALUES ((SELECT id FROM monde WHERE sigle = 'LEIWIS'),'Monocle', 'ILMO', 0.15788915, 15000.00, 'Permet de d�sint�grer ses ennemis d''un seul coup d''oeil');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
    VALUES ((SELECT id FROM monde WHERE sigle = 'LEIWIS'),'Canne � pommeau empoisonn�', 'ILCP', 0.01156894, 12000.00, 'Permet d''empoisonner ses ennemis avec style');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
    VALUES ((SELECT id FROM monde WHERE sigle = 'LEIWIS'),'Pourpoint amain�issant', 'ILPA', 0.15696688, 54000.00, 'V�tement de haut lignage pour bien paraitre au combat');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
    VALUES ((SELECT id FROM monde WHERE sigle = 'LEIWIS'),'Hautes hausses', 'ILHC', 0.38269452, 12300.00, 'Permettent de se t�l�porter tout en gardant la classe');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
    VALUES ((SELECT id FROM monde WHERE sigle = 'LEIWIS'),'Grimoire', 'ILGS', 0.01156489, 12440.00, 'Livre rassemblant le savoir universel');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
    VALUES ((SELECT id FROM monde WHERE sigle = 'LEIWIS'),'Plume enchant�e', 'ILPE', 0.51122365, 59912.00, 'Mise � son chapeau, la plume enchant�e prot�ge des attaques � la t�te');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
    VALUES ((SELECT id FROM monde WHERE sigle = 'LEIWIS'),'Cand�labre', 'ILCA', 0.45411896, 11002.00, 'Plonge tous sauf son possesseur dans le noir');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
    VALUES ((SELECT id FROM monde WHERE sigle = 'LEIWIS'),'Montre � gousset', 'ILMG', 0.36122542, 892.00, 'Sert � retourner le temps');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
    VALUES ((SELECT id FROM monde WHERE sigle = 'LEIWIS'),'Diad�me', 'ILDM', 0.20226361, 10649.00, 'Lorsqu''on aqcui�re le diad�me, nos richesses sont doubl�es');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
    VALUES ((SELECT id FROM monde WHERE sigle = 'LEIWIS'),'�toffe magique', 'ILEM', 0.19144896, 185002.00, 'Immunise son porteur contre trois attaques');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
    VALUES ((SELECT id FROM monde WHERE sigle = 'LEIWIS'),' Miroir de poche', 'ILMP', 0.16159963, 18702.00, 'D�tecte les mensonges');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
    VALUES ((SELECT id FROM monde WHERE sigle = 'LEIWIS'),'Masque de cuivre', 'ILCU', 0.84144848, 13102.00, 'Masque prot�ge contre le poison');

	--Items pour ENDRYA
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
	VALUES ((SELECT id FROM monde WHERE sigle = 'ENDRYA'),'Bikini Cotte de Maille', 'IEBC',0.04021553 , 400000.00, '+ 3 Distraction , + 10 protection ');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
	VALUES ((SELECT id FROM monde WHERE sigle = 'ENDRYA'),'Cape Charme Cr�atures', 'IECC', 0.01155612, 2000000.00, 'Permet de charmer � volont� les cr�atures hostiles ou pas');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
	VALUES ((SELECT id FROM monde WHERE sigle = 'ENDRYA'),'Parchemin de Feu B�ni', 'IEFB', 0.01541598, 45000.00, 'Utilisation unique , cr�e une temp�te de feu');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
	VALUES ((SELECT id FROM monde WHERE sigle = 'ENDRYA'),'Sac sans Fond', 'ISSF', 0.03322161, 2000.00, 'Sac sans fond');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
	VALUES ((SELECT id FROM monde WHERE sigle = 'ENDRYA'),'�p�e du Dragon-D�mon', 'IEDD', 0.03185451, 50000.00, '�p�e +5, avec une chance de maudire les ennemis qu''elle touche');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
	VALUES ((SELECT id FROM monde WHERE sigle = 'ENDRYA'),'Bouclier Repousse Morts-Vivants', 'IEBM',0.07890045, 37500.00, 'Bouclier Repousse Morts-Vivants');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
	VALUES ((SELECT id FROM monde WHERE sigle = 'ENDRYA'),'Plastron de la Destin�e', 'IEPD', 0.09892112, 250000.00, 'Protection +20, augmente les chances d''attirer la b�n�diction divine ');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
	VALUES ((SELECT id FROM monde WHERE sigle = 'ENDRYA'),'Fl�ches Trouve-Cible', 'IEFT',0.04335147, 50000.00, 'Fl�ches qui ne manquent jamais leur cible ');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
	VALUES ((SELECT id FROM monde WHERE sigle = 'ENDRYA'),'Casque de Courage', 'IECD',0.02086950, 40000.00, 'Protection + 5, Bonus d''initiative');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
	VALUES ((SELECT id FROM monde WHERE sigle = 'ENDRYA'),'M�daille du Serpent', 'IEMD', 0.04983215, 20000.00, 'Prot�ge contre le poison');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
	VALUES ((SELECT id FROM monde WHERE sigle = 'ENDRYA'),'Arc Foudroyant de Mayka', 'IEAF',0.03425485, 500000.00, 'Attire la col�re foudroyante de la d�esse sur vos ennemis');
INSERT INTO items_rare (id_monde,nom,sigle,probabilite,cout,description)
	VALUES ((SELECT id FROM monde WHERE sigle = 'ENDRYA'),'Anneau Protection contre le Mal', 'IEAP',0.03466118 , 800.00, 'Bonus contre les cr�atures d�moniaques');

PROMPT  
PROMPT =======================================================
PROMPT Insertions du premier joueur et de son avatar
PROMPT =======================================================

INSERT INTO joueur(id, alias, courriel, mot_de_passe, genre, date_inscription, date_de_naissance)
    VALUES (seq_joueur_id.NEXTVAL,'RoboRidley','danick.massicotte@gmail.com', 'yhnmju678', 'M', TO_DATE('2018-10-02', 'YYYY-MM-DD'), TO_DATE('1987-11-14', 'YYYY-MM-DD'));
    
-- Avatar principal
INSERT INTO avatar(id, id_joueur, nom, date_creation, niveau, qte_mox) 
    VALUES (seq_avatar_id.NEXTVAL, (SELECT id FROM joueur WHERE alias = 'RoboRidley'), 'UltraNova', TO_DATE('2018-10-02', 'YYYY-MM-DD'), 35, 500);     
    
INSERT INTO couleur_preferee(id_avatar, red, green, blue) 
    VALUES ((SELECT id FROM avatar WHERE nom = 'UltraNova'), 192, 192, 192); 
INSERT INTO couleur_preferee(id_avatar, red, green, blue) 
    VALUES ((SELECT id FROM avatar WHERE nom = 'UltraNova'), 0, 0, 0);

INSERT INTO phrases_perso(phrases, id_avatar) 
    VALUES ('Gotta go fast', (SELECT id FROM avatar WHERE nom = 'UltraNova'));
INSERT INTO phrases_perso(phrases, id_avatar) 
    VALUES ('You just got wrecked!', (SELECT id FROM avatar WHERE nom = 'UltraNova'));
INSERT INTO phrases_perso(phrases, id_avatar) 
    VALUES ('Great power, responsability... stuff', (SELECT id FROM avatar WHERE nom = 'UltraNova'));    
    
PROMPT  
PROMPT =======================================================
PROMPT Insertions du deuxi�me joueur et de ses avatars
PROMPT =======================================================    
    
INSERT INTO joueur(id, alias, courriel, mot_de_passe, genre, date_inscription, date_de_naissance)
    VALUES (seq_joueur_id.NEXTVAL, 'JeanChristo*', 'jcdemers@gmail.com', 'qwerty1234', 'M', TO_DATE('2018-10-02', 'YYYY-MM-DD'), TO_DATE('1980-01-01', 'YYYY-MM-DD'));

-- Avatar principal
INSERT INTO avatar(id, id_joueur, nom, date_creation, niveau, qte_mox)
    VALUES (seq_avatar_id.NEXTVAL, (SELECT id FROM joueur WHERE alias = 'JeanChristo*'), 'JC_Belmont*', TO_DATE('2018-10-02', 'YYYY-MM-DD'), 89, 1000);
	
INSERT INTO couleur_preferee(id_avatar, red, green, blue) 
    VALUES ((SELECT id FROM avatar WHERE nom = 'JC_Belmont*'), 60, 0, 0);
INSERT INTO couleur_preferee(id_avatar, red, green, blue) 
    VALUES ((SELECT id FROM avatar WHERE nom = 'JC_Belmont*'), 21, 27, 31);

INSERT INTO phrases_perso(phrases, id_avatar) 
    VALUES ('Arri�re, sorci�re!', (SELECT id FROM avatar WHERE nom = 'JC_Belmont*'));
INSERT INTO phrases_perso(phrases, id_avatar) 
    VALUES ('Les vampires sont les pires', (SELECT id FROM avatar WHERE nom = 'JC_Belmont*'));
INSERT INTO phrases_perso(phrases, id_avatar) 
    VALUES ('Un coup; un loup.', (SELECT id FROM avatar WHERE nom = 'JC_Belmont*'));
INSERT INTO phrases_perso(phrases, id_avatar)
	VALUES ('Y''en a marre de ces cauchemards.', (SELECT id FROM avatar WHERE nom = 'JC_Belmont*'));
INSERT INTO phrases_perso(phrases, id_avatar) 
    VALUES ('C''est l''heure de la chasse...', (SELECT id FROM avatar WHERE nom = 'JC_Belmont*'));

-- Deuxi�me avatar
INSERT INTO avatar(id, id_joueur, nom, date_creation, niveau, qte_mox)
    VALUES (seq_avatar_id.NEXTVAL, (SELECT id FROM joueur WHERE alias = 'JeanChristo*'), 'Super-Teacher', TO_DATE('2018-10-02', 'YYYY-MM-DD'), 62, 750);
	
INSERT INTO couleur_preferee(id_avatar, red, green, blue)
    VALUES ((SELECT id FROM avatar WHERE nom = 'Super-Teacher'), 0, 255, 17);
INSERT INTO couleur_preferee(id_avatar, red, green, blue)
    VALUES ((SELECT id FROM avatar WHERE nom = 'Super-Teacher'), 255, 255, 255);
INSERT INTO couleur_preferee(id_avatar, red, green, blue)
    VALUES ((SELECT id FROM avatar WHERE nom = 'Super-Teacher'), 0, 111, 255);

INSERT INTO phrases_perso(phrases, id_avatar)
    VALUES ('Je vais t''apprendre!', (SELECT id FROM avatar WHERE nom = 'Super-Teacher'));
INSERT INTO phrases_perso(phrases, id_avatar)
    VALUES ('''E'' pour �CHEC TOTAL', (SELECT id FROM avatar WHERE nom = 'Super-Teacher'));
INSERT INTO phrases_perso(phrases, id_avatar)
    VALUES ('SGBD: Super Grosse Bombe Destructrice!', (SELECT id FROM avatar WHERE nom = 'Super-Teacher'));

-- Troisi�me avatar
INSERT INTO avatar(id, id_joueur, nom, date_creation, niveau, qte_mox)
    VALUES (seq_avatar_id.NEXTVAL, (SELECT id FROM joueur WHERE alias = 'JeanChristo*'), 'OrcKiller979', TO_DATE('2018-10-02', 'YYYY-MM-DD'), 35, 500);
	
INSERT INTO couleur_preferee(id_avatar, red, green, blue)
    VALUES ((SELECT id FROM avatar WHERE nom = 'OrcKiller979'), 0, 0, 0);

INSERT INTO phrases_perso(phrases, id_avatar)
    VALUES ('...', (SELECT id FROM avatar WHERE nom = 'OrcKiller979'));
  

PROMPT  
PROMPT =======================================================
PROMPT Insertions du troisi�me joueur et de son avatar
PROMPT =======================================================  

INSERT INTO joueur(id, alias, courriel, mot_de_passe, genre, date_inscription, date_de_naissance) 
    VALUES(seq_joueur_id.NEXTVAL,'JiciTroy450', 'jici_troy@hotmail.com', 'secret1', 'M',TO_DATE('2018-10-02', 'YYYY-MM-DD'), TO_DATE('18/02/1981', 'DD/MM/YYYY'));
     
INSERT INTO avatar(id, id_joueur, nom, date_creation, niveau, qte_mox)
    VALUES(seq_avatar_id.NEXTVAL,(SELECT id FROM joueur WHERE alias = 'JiciTroy450'), 'ElPredatore', TO_DATE('2018-10-02', 'YYYY-MM-DD'),1, 0);
    
INSERT INTO couleur_preferee(id_avatar, red, green, blue) 
    VALUES ((SELECT id FROM avatar WHERE nom = 'ElPredatore'), 123, 244, 65); 
INSERT INTO couleur_preferee(id_avatar, red, green, blue) 
    VALUES ((SELECT id FROM avatar WHERE nom = 'ElPredatore'), 87, 111, 231);

INSERT INTO phrases_perso(phrases, id_avatar) 
    VALUES ('Say hello to papa!', (SELECT id FROM avatar WHERE nom = 'ElPredatore'));
INSERT INTO phrases_perso(phrases, id_avatar) 
    VALUES ('Good..bad...I''m the guy with the gun', (SELECT id FROM avatar WHERE nom = 'ElPredatore'));
INSERT INTO phrases_perso(phrases, id_avatar) 
    VALUES ('Say uncle!', (SELECT id FROM avatar WHERE nom = 'ElPredatore'));
    
PROMPT  
PROMPT =======================================================
PROMPT Insertions du quatri�me joueur et de son avatar
PROMPT =======================================================  

INSERT INTO joueur(id, alias, courriel, mot_de_passe, genre, date_inscription, date_de_naissance)
    VALUES (seq_joueur_id.NEXTVAL,'YayaZoya','jasmine.kaddouri@gmail.com', 'MotMagique', 'F', TO_DATE('2018-10-02', 'YYYY-MM-DD'), TO_DATE('1993-03-23', 'YYYY-MM-DD'));
    
-- Avatar principal
INSERT INTO avatar(id, id_joueur, nom, date_creation, niveau, qte_mox) 
    VALUES (seq_avatar_id.NEXTVAL, (SELECT id FROM joueur WHERE alias = 'YayaZoya'), 'BelAmi', TO_DATE('2018-10-02', 'YYYY-MM-DD'), 12, 50);     
    
INSERT INTO couleur_preferee(id_avatar, red, green, blue) 
    VALUES ((SELECT id FROM avatar WHERE nom = 'BelAmi'), 250, 1, 33); 
INSERT INTO couleur_preferee(id_avatar, red, green, blue) 
    VALUES ((SELECT id FROM avatar WHERE nom = 'BelAmi'), 0, 120, 0);
INSERT INTO couleur_preferee(id_avatar, red, green, blue) 
    VALUES ((SELECT id FROM avatar WHERE nom = 'BelAmi'), 218, 1, 250);   

INSERT INTO phrases_perso(phrases, id_avatar) 
    VALUES ('There is champagne bubbles that are faster than thou ', (SELECT id FROM avatar WHERE nom = 'BelAmi'));
INSERT INTO phrases_perso(phrases, id_avatar) 
    VALUES (' Plus de morts, moins d''ennemis!', (SELECT id FROM avatar WHERE nom = 'BelAmi'));
INSERT INTO phrases_perso(phrases, id_avatar) 
    VALUES ('Tuez-moi cet ennemi que je ne saurai voir', (SELECT id FROM avatar WHERE nom = 'BelAmi')); 

PROMPT  
PROMPT =======================================================
PROMPT Insertions du cinqui�me joueur et de son avatar
PROMPT =======================================================  

INSERT INTO joueur (id, alias, courriel, mot_de_passe, genre, date_inscription, date_de_naissance)
	VALUES (seq_joueur_id.NEXTVAL,'KalamityKat', 'Mlle.lyla@gmail.com', 'AAAaaaa111', 'F', TO_DATE('2018-10-02', 'YYYY-MM-DD'), TO_DATE('1983-01-30', 'YYYY-MM-DD'));

INSERT INTO avatar (id, id_joueur, nom, date_creation, niveau, qte_mox) 
	VALUES (seq_avatar_id.NEXTVAL,(SELECT id FROM joueur WHERE alias = 'KalamityKat'), 'Shaazaah', TO_DATE('2018-10-02', 'YYYY-MM-DD'),50, 0);
	
INSERT INTO couleur_preferee (id_avatar, red, green, blue)
VALUES ((SELECT id FROM avatar WHERE nom = 'Shaazaah'),255,0, 0);

INSERT INTO couleur_preferee (id_avatar, red, green, blue)
VALUES ((SELECT id FROM avatar WHERE nom = 'Shaazaah'), 0, 0, 255);

INSERT INTO phrases_perso (phrases, id_avatar)
VALUES ('Cette fl�che a ton nom dessus!', (SELECT id FROM avatar WHERE nom = 'Shaazaah'));

INSERT INTO phrases_perso (phrases, id_avatar)
VALUES ('Br�le, engeance d�moniaque!', (SELECT id FROM avatar WHERE nom = 'Shaazaah'));

INSERT INTO phrases_perso (phrases, id_avatar)
VALUES ('Reste mort et ne te rel�ves plus !', (SELECT id FROM avatar WHERE nom = 'Shaazaah'));

PROMPT   
PROMPT =======================================================
PROMPT Insertions des activit�s du premier joueur
PROMPT =======================================================

-- Premi�re activit�
INSERT INTO activite(id_joueur, date_heure_debut, duree) 
	VALUES ((SELECT id FROM joueur WHERE alias = 'RoboRidley'),
	TO_DATE('2018-10-28 19:30', 'YYYY-MM-DD HH24:MI'),
	21600);
	
INSERT INTO activite_avatar(id_activite, id_avatar, duree)
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'RoboRidley')
		AND date_heure_debut = TO_DATE('2018-10-28 19:30', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM avatar WHERE nom = 'UltraNova'),
	21600);
	
INSERT INTO activite_monde(id_activite, id_monde, duree) 
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'RoboRidley')
		AND date_heure_debut = TO_DATE('2018-10-28 19:30', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM monde WHERE sigle = 'HROPLS'),
	21600);
      
-- Deuxi�me activit� 
INSERT INTO activite(id_joueur, date_heure_debut, duree) 
	VALUES ((SELECT id FROM joueur WHERE alias = 'RoboRidley'),
	TO_DATE('2018-10-29 13:03', 'YYYY-MM-DD HH24:MI'),
	30600);
	
INSERT INTO activite_avatar(id_activite, id_avatar, duree)
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'RoboRidley')
		AND date_heure_debut = TO_DATE('2018-10-29 13:03', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM avatar WHERE nom = 'UltraNova'),
	30600);
	
INSERT INTO activite_monde(id_activite, id_monde, duree)
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'RoboRidley')
		AND date_heure_debut = TO_DATE('2018-10-29 13:03', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM monde WHERE sigle = 'HROPLS'),
	10200);
	
INSERT INTO activite_monde(id_activite, id_monde, duree) 
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'RoboRidley')
		AND date_heure_debut = TO_DATE('2018-10-29 13:03', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM monde WHERE sigle = 'AVACYN'),
	20400);
	
-- Troisi�me activit�
INSERT INTO activite(id_joueur, date_heure_debut, duree) 
	VALUES ((SELECT id FROM joueur WHERE alias = 'RoboRidley'),
	TO_DATE('2018-10-30 16:18', 'YYYY-MM-DD HH24:MI'), 11700);

INSERT INTO activite_avatar(id_activite, id_avatar, duree) 
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'RoboRidley')
		AND date_heure_debut = TO_DATE('2018-10-30 16:18', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM avatar WHERE nom = 'UltraNova'),
	11700);
	
INSERT INTO activite_monde(id_activite, id_monde, duree) 
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'RoboRidley')
		AND date_heure_debut = TO_DATE('2018-10-30 16:18', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM monde WHERE sigle = 'HROPLS'),
	1800);
	
INSERT INTO activite_monde(id_activite, id_monde, duree)
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'RoboRidley')
		AND date_heure_debut = TO_DATE('2018-10-30 16:18', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM monde WHERE sigle = 'ENDRYA'),
	7200);
	
INSERT INTO activite_monde(id_activite, id_monde, duree)
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'RoboRidley')
		AND date_heure_debut = TO_DATE('2018-10-30 16:18', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM monde WHERE sigle = 'HROPLS'),
	2700);
	
    
--Items obtenus 
INSERT INTO items_obtenus(id_avatar, id_items_rare, date_heure_obtention, quantite)
    VALUES ((SELECT id FROM avatar WHERE nom = 'UltraNova'),(SELECT id FROM items_rare WHERE sigle = 'IHSG'), 
        TO_DATE('2018-10-28 21:02', 'YYYY-MM-DD HH24:MI'),1);
		
INSERT INTO items_obtenus(id_avatar, id_items_rare, date_heure_obtention, quantite)
    VALUES ((SELECT id FROM avatar WHERE nom = 'UltraNova'),(SELECT id FROM items_rare WHERE sigle = 'IACP'), 
        TO_DATE('2018-10-29 16:03', 'YYYY-MM-DD HH24:MI'),1);   

INSERT INTO items_obtenus(id_avatar, id_items_rare, date_heure_obtention, quantite)
    VALUES ((SELECT id FROM avatar WHERE nom = 'UltraNova'),(SELECT id FROM items_rare WHERE sigle = 'IECC'), 
        TO_DATE('2018-10-30 16:54', 'YYYY-MM-DD HH24:MI'),1);
      
--Caract�ristiques obtenues
INSERT INTO carac_avatar(id_avatar, id_carac_monde, date_obtention, niveau) 
	VALUES ((SELECT id FROM avatar WHERE nom = 'UltraNova'),(SELECT id FROM carac_monde WHERE sigle = 'FIN'),
	TO_DATE('2018-10-29 16:45', 'YYYY-MM-DD HH24:MI'), 70
	);

 INSERT INTO carac_avatar(id_avatar, id_carac_monde, date_obtention, niveau) 
	VALUES ((SELECT id FROM avatar WHERE nom = 'UltraNova'),(SELECT id FROM carac_monde WHERE sigle = 'FHT'),
	TO_DATE('2018-10-30 16:32', 'YYYY-MM-DD HH24:MI'), 89
	);
    
PROMPT  
PROMPT ===============================================================
PROMPT Insertions des activit�s pour le deuxi�me joueur  * PRINCIPAL 
PROMPT ===============================================================

-- Premi�re activit�
INSERT INTO activite(id_joueur, date_heure_debut, duree)
	VALUES ((SELECT id FROM joueur WHERE alias = 'JeanChristo*'),
	TO_DATE('2018-10-28 21:43', 'YYYY-MM-DD HH24:MI'),
	3600);
	
INSERT INTO activite_avatar(id_activite, id_avatar, duree) 
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JeanChristo*')
		AND date_heure_debut = TO_DATE('2018-10-28 21:43', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM avatar WHERE nom = 'OrcKiller979'),
	3600);
	
INSERT INTO activite_monde(id_activite, id_monde, duree)
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JeanChristo*')
		AND date_heure_debut = TO_DATE('2018-10-28 21:43', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM monde WHERE sigle = 'ENDRYA'),
	3600);

-- Deuxi�me activit�
INSERT INTO activite(id_joueur, date_heure_debut, duree)
	VALUES ((SELECT id FROM joueur WHERE alias = 'JeanChristo*'),
	TO_DATE('2018-10-29 09:31', 'YYYY-MM-DD HH24:MI'),
	10800);
	
INSERT INTO activite_avatar(id_activite, id_avatar, duree)
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JeanChristo*')
		AND date_heure_debut = TO_DATE('2018-10-29 09:31', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM avatar WHERE nom = 'JC_Belmont*'),
	10800);
	
INSERT INTO activite_monde(id_activite, id_monde, duree)
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JeanChristo*')
		AND date_heure_debut = TO_DATE('2018-10-29 09:31', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM monde WHERE sigle = 'HROPLS'),
	10800);

--Ajout d'une caract�ristique obtenue
INSERT INTO carac_avatar(id_avatar, id_carac_monde, date_obtention, niveau) 
	VALUES ((SELECT id FROM avatar WHERE nom = 'JC_Belmont*'),(SELECT id FROM carac_monde WHERE sigle = 'FHF'),
	TO_DATE('2018-10-29 09:42', 'YYYY-MM-DD HH24:MI'), 13);  

-- Troisi�me activit�
INSERT INTO activite(id_joueur, date_heure_debut, duree)
	VALUES ((SELECT id FROM joueur WHERE alias = 'JeanChristo*'),
	TO_DATE('2018-10-29 15:11', 'YYYY-MM-DD HH24:MI'),
	5400);
	
INSERT INTO activite_avatar(id_activite, id_avatar, duree)
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JeanChristo*')
		AND date_heure_debut = TO_DATE('2018-10-29 15:11', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM avatar WHERE nom = 'JC_Belmont*'),
	5400);
	
INSERT INTO activite_monde(id_activite, id_monde, duree)
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JeanChristo*')
		AND date_heure_debut = TO_DATE('2018-10-29 15:11', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM monde WHERE sigle = 'AVACYN'),
	5400);

INSERT INTO carac_avatar(id_avatar, id_carac_monde, date_obtention, niveau) 
	VALUES ((SELECT id FROM avatar WHERE nom = 'JC_Belmont*'),(SELECT id FROM carac_monde WHERE sigle = 'FIN'),
	TO_DATE('2018-10-29 15:44', 'YYYY-MM-DD HH24:MI'), 13);  

-- Quatri�me activit�
INSERT INTO activite(id_joueur, date_heure_debut, duree)
	VALUES ((SELECT id FROM joueur WHERE alias = 'JeanChristo*'),
	TO_DATE('2018-10-29 22:58', 'YYYY-MM-DD HH24:MI'),
	3600);
	
INSERT INTO activite_avatar(id_activite, id_avatar, duree)
VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JeanChristo*')
		AND date_heure_debut = TO_DATE('2018-10-29 22:58', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM avatar WHERE nom = 'JC_Belmont*'),
	3600);
	
INSERT INTO activite_monde(id_activite, id_monde, duree)
VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JeanChristo*')
		AND date_heure_debut = TO_DATE('2018-10-29 22:58', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM monde WHERE sigle = 'AVACYN'),
	3600);

-- Cinqui�me activit�
INSERT INTO activite(id_joueur, date_heure_debut, duree)
	VALUES ((SELECT id FROM joueur WHERE alias = 'JeanChristo*'),
	TO_DATE('2018-11-02 21:02', 'YYYY-MM-DD HH24:MI'),
	8100);
	
INSERT INTO activite_avatar(id_activite, id_avatar, duree)
VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JeanChristo*')
		AND date_heure_debut = TO_DATE('2018-11-02 21:02', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM avatar WHERE nom = 'JC_Belmont*'),
	8100);
	
INSERT INTO activite_monde(id_activite, id_monde, duree)
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JeanChristo*')
		AND date_heure_debut = TO_DATE('2018-11-02 21:02', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM monde WHERE sigle = 'AVACYN'),
	8100);

-- Sixi�me activit�
INSERT INTO activite(id_joueur, date_heure_debut, duree)
	VALUES ((SELECT id FROM joueur WHERE alias = 'JeanChristo*'),
	TO_DATE('2018-11-06 12:54', 'YYYY-MM-DD HH24:MI'),
	36000);
	
INSERT INTO activite_avatar(id_activite, id_avatar, duree)
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JeanChristo*')
		AND date_heure_debut = TO_DATE('2018-11-06 12:54', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM avatar WHERE nom = 'JC_Belmont*'),
	36000);
	
INSERT INTO activite_monde(id_activite, id_monde, duree)
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JeanChristo*')
		AND date_heure_debut = TO_DATE('2018-11-06 12:54', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM monde WHERE sigle = 'ENDRYA'),
	5400);
    
INSERT INTO carac_avatar(id_avatar, id_carac_monde, date_obtention, niveau) 
	VALUES ((SELECT id FROM avatar WHERE nom = 'JC_Belmont*'),(SELECT id FROM carac_monde WHERE sigle = 'FEL'),
	TO_DATE('2018-11-06 13:20', 'YYYY-MM-DD HH24:MI'), 13);      

	
INSERT INTO activite_monde(id_activite, id_monde, duree)
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JeanChristo*')
		AND date_heure_debut = TO_DATE('2018-11-06 12:54', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM monde WHERE sigle = 'AVACYN'),
	18000);
	
INSERT INTO activite_monde(id_activite, id_monde, duree)
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JeanChristo*')
		AND date_heure_debut = TO_DATE('2018-11-06 12:54', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM monde WHERE sigle = 'LEIWIS'),
	6300);
	
INSERT INTO activite_monde(id_activite, id_monde, duree)
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JeanChristo*')
		AND date_heure_debut = TO_DATE('2018-11-06 12:54', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM monde WHERE sigle = 'HROPLS'),
	6300);

-- Septi�me activit�
INSERT INTO activite(id_joueur, date_heure_debut, duree)
	VALUES ((SELECT id FROM joueur WHERE alias = 'JeanChristo*'),
	TO_DATE('2018-11-07 14:44', 'YYYY-MM-DD HH24:MI'),
	7200);
	
INSERT INTO activite_avatar(id_activite, id_avatar, duree)
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JeanChristo*')
		AND date_heure_debut = TO_DATE('2018-11-07 14:44', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM avatar WHERE nom = 'JC_Belmont*'),
	7200);
	
INSERT INTO activite_monde(id_activite, id_monde, duree)
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JeanChristo*')
		AND date_heure_debut = TO_DATE('2018-11-07 14:44', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM monde WHERE sigle = 'ENDRYA'),
	7200);

-- Huiti�me activit�
INSERT INTO activite(id_joueur, date_heure_debut, duree)
	VALUES ((SELECT id FROM joueur WHERE alias = 'JeanChristo*'),
	TO_DATE('2018-11-07 20:31', 'YYYY-MM-DD HH24:MI'),
	7200);
	
INSERT INTO activite_avatar(id_activite, id_avatar, duree)
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JeanChristo*')
		AND date_heure_debut = TO_DATE('2018-11-07 20:31', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM avatar WHERE nom = 'JC_Belmont*'),
	7200);
	
INSERT INTO activite_monde(id_activite, id_monde, duree)
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JeanChristo*')
		AND date_heure_debut = TO_DATE('2018-11-07 20:31', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM monde WHERE sigle = 'ENDRYA'),
	7200);

-- Neuvi�me activit�
INSERT INTO activite(id_joueur, date_heure_debut, duree)
	VALUES ((SELECT id FROM joueur WHERE alias = 'JeanChristo*'),
	TO_DATE('2018-11-28 09:20', 'YYYY-MM-DD HH24:MI'),
	12600);
	
INSERT INTO activite_avatar(id_activite, id_avatar, duree)
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JeanChristo*')
		AND date_heure_debut = TO_DATE('2018-11-28 09:20', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM avatar WHERE nom = 'JC_Belmont*'),
	12600);
	
INSERT INTO activite_monde(id_activite, id_monde, duree)
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JeanChristo*')
		AND date_heure_debut = TO_DATE('2018-11-28 09:20', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM monde WHERE sigle = 'AVACYN'),
	12600);

-- Dixi�me activit�
INSERT INTO activite(id_joueur, date_heure_debut, duree)
	VALUES ((SELECT id FROM joueur WHERE alias = 'JeanChristo*'),
	TO_DATE('2018-11-28 20:08', 'YYYY-MM-DD HH24:MI'),
	8500);
	
INSERT INTO activite_avatar(id_activite, id_avatar, duree)
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JeanChristo*')
		AND date_heure_debut = TO_DATE('2018-11-28 20:08', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM avatar WHERE nom = 'Super-Teacher'),
	3600);
    
--achat d'un item
INSERT INTO achat(id_joueur, date_paiement, id_type_achat, id_mode_paiement, id_item_rare, montant, date_debut, date_fin) 
	VALUES ((SELECT id FROM joueur WHERE alias = 'JeanChristo*'),
	TO_DATE('2018-11-28 20:14', 'YYYY-MM-DD HH24:MI'), 2, 2, (SELECT id FROM items_rare WHERE sigle = 'ILPA'),4.99, NULL, NULL);	

-- Ajout de l'item dans la table items_obtenus
INSERT INTO items_obtenus(id_avatar, id_items_rare, date_heure_obtention, quantite)
    VALUES ((SELECT id FROM avatar WHERE nom = 'Super-Teacher'),(SELECT id FROM items_rare WHERE sigle = 'ILPA'), 
        TO_DATE('2018-11-28 20:14', 'YYYY-MM-DD HH24:MI'),1);


INSERT INTO activite_monde(id_activite, id_monde, duree)
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JeanChristo*')
		AND date_heure_debut = TO_DATE('2018-11-28 20:08', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM monde WHERE sigle = 'HROPLS'),
	1300);
       
--Ajout d'une caract�ristique obtenue
INSERT INTO carac_avatar(id_avatar, id_carac_monde, date_obtention, niveau) 
	VALUES ((SELECT id FROM avatar WHERE nom = 'Super-Teacher'),(SELECT id FROM carac_monde WHERE sigle = 'FHF'),
	TO_DATE('2018-11-28 20:20', 'YYYY-MM-DD HH24:MI'), 13);   
        
INSERT INTO activite_monde(id_activite, id_monde, duree)
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JeanChristo*')
		AND date_heure_debut = TO_DATE('2018-11-28 20:08', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM monde WHERE sigle = 'LEIWIS'),
	1300);
    
INSERT INTO carac_avatar(id_avatar, id_carac_monde, date_obtention, niveau) 
	VALUES ((SELECT id FROM avatar WHERE nom = 'Super-Teacher'),(SELECT id FROM carac_monde WHERE sigle = 'FLI'),
	TO_DATE('2018-11-28 20:20', 'YYYY-MM-DD HH24:MI'), 13);    
    
    
INSERT INTO carac_avatar(id_avatar, id_carac_monde, date_obtention, niveau) 
	VALUES ((SELECT id FROM avatar WHERE nom = 'Super-Teacher'),(SELECT id FROM carac_monde WHERE sigle = 'FLC'),
	TO_DATE('2018-11-28 20:20', 'YYYY-MM-DD HH24:MI'), 13);  
    
INSERT INTO activite_avatar(id_activite, id_avatar, duree)
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JeanChristo*')
		AND date_heure_debut = TO_DATE('2018-11-28 20:08', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM avatar WHERE nom = 'OrcKiller979'),
	3600);

INSERT INTO carac_avatar(id_avatar, id_carac_monde, date_obtention, niveau) 
	VALUES ((SELECT id FROM avatar WHERE nom = 'OrcKiller979'),(SELECT id FROM carac_monde WHERE sigle = 'FEG'),
	TO_DATE('2018-11-28 20:08', 'YYYY-MM-DD HH24:MI'), 64
	);

 INSERT INTO carac_avatar(id_avatar, id_carac_monde, date_obtention, niveau) 
	VALUES ((SELECT id FROM avatar WHERE nom = 'OrcKiller979'),(SELECT id FROM carac_monde WHERE sigle = 'FEL'),
	TO_DATE('2018-11-28 20:08', 'YYYY-MM-DD HH24:MI'), 89
	);


INSERT INTO activite_monde(id_activite, id_monde, duree)
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JeanChristo*')
		AND date_heure_debut = TO_DATE('2018-11-28 20:08', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM monde WHERE sigle = 'ENDRYA'),
	7200);

-- Onzi�me activit�
INSERT INTO activite(id_joueur, date_heure_debut, duree)
	VALUES ((SELECT id FROM joueur WHERE alias = 'JeanChristo*'),
	TO_DATE('2018-12-03 13:36', 'YYYY-MM-DD HH24:MI'),
	9000);
	
INSERT INTO activite_avatar(id_activite, id_avatar, duree)
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JeanChristo*')
		AND date_heure_debut = TO_DATE('2018-12-03 13:36', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM avatar WHERE nom = 'JC_Belmont*'),
	9000);
	
INSERT INTO activite_monde(id_activite, id_monde, duree)
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JeanChristo*')
		AND date_heure_debut = TO_DATE('2018-12-03 13:36', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM monde WHERE sigle = 'AVACYN'),
	9000);
    
--Ajout d'une caract�ristique obtenue
INSERT INTO carac_avatar(id_avatar, id_carac_monde, date_obtention, niveau) 
	VALUES ((SELECT id FROM avatar WHERE nom = 'JC_Belmont*'),(SELECT id FROM carac_monde WHERE sigle = 'FVI'),
	TO_DATE('2018-12-03 13:45', 'YYYY-MM-DD HH24:MI'), 13);   

-- Douzi�me activit�
INSERT INTO activite(id_joueur, date_heure_debut, duree)
	VALUES ((SELECT id FROM joueur WHERE alias = 'JeanChristo*'),
	TO_DATE('2018-12-04 10:44', 'YYYY-MM-DD HH24:MI'),
	600);
	
INSERT INTO activite_avatar(id_activite, id_avatar, duree)
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JeanChristo*')
		AND date_heure_debut = TO_DATE('2018-12-04 10:44', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM avatar WHERE nom = 'JC_Belmont*'),
	600);
	
INSERT INTO activite_monde(id_activite, id_monde, duree)
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JeanChristo*')
		AND date_heure_debut = TO_DATE('2018-12-04 10:44', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM monde WHERE sigle = 'AVACYN'),
	600);
	

INSERT INTO items_obtenus(id_avatar, id_items_rare, date_heure_obtention, quantite)
    VALUES ((SELECT id FROM avatar WHERE nom = 'OrcKiller979'),(SELECT id FROM items_rare WHERE sigle = 'IEBC'), 
        TO_DATE('2018-10-28 22:10', 'YYYY-MM-DD HH24:MI'),1);   

INSERT INTO items_obtenus(id_avatar, id_items_rare, date_heure_obtention, quantite)
    VALUES ((SELECT id FROM avatar WHERE nom = 'OrcKiller979'),(SELECT id FROM items_rare WHERE sigle = 'IEDD'), 
        TO_DATE('2018-10-28 22:32', 'YYYY-MM-DD HH24:MI'),1); 

INSERT INTO items_obtenus(id_avatar, id_items_rare, date_heure_obtention, quantite)
    VALUES ((SELECT id FROM avatar WHERE nom = 'JC_Belmont*'),(SELECT id FROM items_rare WHERE sigle = 'IHJP'), 
        TO_DATE('2018-10-29 10:05', 'YYYY-MM-DD HH24:MI'),1); 
		
INSERT INTO items_obtenus(id_avatar, id_items_rare, date_heure_obtention, quantite)
    VALUES ((SELECT id FROM avatar WHERE nom = 'JC_Belmont*'),(SELECT id FROM items_rare WHERE sigle = 'IHMA'), 
        TO_DATE('2018-10-29 11:25', 'YYYY-MM-DD HH24:MI'),1); 
		
INSERT INTO items_obtenus(id_avatar, id_items_rare, date_heure_obtention, quantite)
    VALUES ((SELECT id FROM avatar WHERE nom = 'JC_Belmont*'),(SELECT id FROM items_rare WHERE sigle = 'IHSG'), 
        TO_DATE('2018-10-29 12:02', 'YYYY-MM-DD HH24:MI'),1); 

INSERT INTO items_obtenus(id_avatar, id_items_rare, date_heure_obtention, quantite)
    VALUES ((SELECT id FROM avatar WHERE nom = 'JC_Belmont*'),(SELECT id FROM items_rare WHERE sigle = 'IHJP'), 
        TO_DATE('2018-10-29 10:05', 'YYYY-MM-DD HH24:MI'),1); 

INSERT INTO items_obtenus(id_avatar, id_items_rare, date_heure_obtention, quantite)
    VALUES ((SELECT id FROM avatar WHERE nom = 'JC_Belmont*'),(SELECT id FROM items_rare WHERE sigle = 'IECD'), 
        TO_DATE('2018-11-06 13:40', 'YYYY-MM-DD HH24:MI'),1); 
		
INSERT INTO items_obtenus(id_avatar, id_items_rare, date_heure_obtention, quantite)
    VALUES ((SELECT id FROM avatar WHERE nom = 'JC_Belmont*'),(SELECT id FROM items_rare WHERE sigle = 'IARB'), 
        TO_DATE('2018-12-03 13:45', 'YYYY-MM-DD HH24:MI'),1); 

PROMPT =======================================================
PROMPT Insertions des activit�s du joueur JiciTroy450
PROMPT =======================================================

-- 1ere ACTIVIT� ***************************************************************************************************** 

-- Achat abonnement
INSERT INTO achat(id_joueur, date_paiement, id_type_achat, id_mode_paiement, id_item_rare, montant, date_debut, date_fin) 
	VALUES ((SELECT id FROM joueur WHERE alias = 'JiciTroy450'),
	TO_DATE('2018-11-01 8:57', 'YYYY-MM-DD HH24:MI'), 1, 1, Null, 29.99, TO_DATE('2018-11-01', 'YYYY-MM-DD'),TO_DATE('2018-11-30', 'YYYY-MM-DD'));

-- Login / logout du joueur
INSERT INTO activite(id_joueur, date_heure_debut, duree) 
	VALUES ((SELECT id FROM joueur WHERE alias = 'JiciTroy450'),
	TO_DATE('2018-11-01 8:52', 'YYYY-MM-DD HH24:MI'),7258);

-- Login / logout par avatar
INSERT INTO activite_avatar(id_activite, id_avatar, duree) 
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JiciTroy450') AND activite.date_heure_debut = TO_DATE('2018-11-01 8:52', 'YYYY-MM-DD HH24:MI')),
        (SELECT id FROM avatar WHERE nom = 'ElPredatore'),7258);

-- Login / logout par monde	
INSERT INTO activite_monde(id_activite, id_monde, duree) 
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JiciTroy450')),
        (SELECT id FROM monde WHERE sigle = 'AVACYN'),4034);
        
INSERT INTO activite_monde(id_activite, id_monde, duree) 
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JiciTroy450')),
        (SELECT id FROM monde WHERE sigle = 'HROPLS'),3224);

-- Obtention d'une caract�ristique        
INSERT INTO carac_avatar(id_avatar, id_carac_monde, date_obtention, niveau) 
	VALUES ((SELECT id FROM avatar WHERE nom = 'ElPredatore'),(SELECT id FROM carac_monde WHERE sigle = 'FIN'),
	TO_DATE('2018-11-01 9:12', 'YYYY-MM-DD HH24:MI'), 1);
    
-- Achat d'un item
INSERT INTO achat(id_joueur, date_paiement, id_type_achat, id_mode_paiement, id_item_rare, montant, date_debut, date_fin) 
	VALUES ((SELECT id FROM joueur WHERE alias = 'JiciTroy450'),
	TO_DATE('2018-11-01 9:15', 'YYYY-MM-DD HH24:MI'), 2, 2, (SELECT id FROM items_rare WHERE sigle = 'IACP'), 0.99, NULL, NULL);

-- Ajout de l'item dans la table items_obtenus
INSERT INTO items_obtenus(id_avatar, id_items_rare, date_heure_obtention, quantite)
    VALUES ((SELECT id FROM avatar WHERE nom = 'ElPredatore'),(SELECT id FROM items_rare WHERE sigle = 'IACP'), 
        TO_DATE('2018-11-01 9:15', 'YYYY-MM-DD HH24:MI'),10);
        
-- Obtention d'items
INSERT INTO items_obtenus(id_avatar, id_items_rare, date_heure_obtention, quantite)
    VALUES ((SELECT id FROM avatar WHERE nom = 'ElPredatore'),(SELECT id FROM items_rare WHERE sigle = 'IAEB'), 
        TO_DATE('2018-11-01 9:22', 'YYYY-MM-DD HH24:MI'),1);
        
INSERT INTO items_obtenus(id_avatar, id_items_rare, date_heure_obtention, quantite)
    VALUES ((SELECT id FROM avatar WHERE nom = 'ElPredatore'),(SELECT id FROM items_rare WHERE sigle = 'IHJP'), 
        TO_DATE('2018-11-01 10:20', 'YYYY-MM-DD HH24:MI'),1);


        
-- 2e ACTIVIT� ***************************************************************************************************** 
INSERT INTO activite(id_joueur, date_heure_debut, duree) 
	VALUES ((SELECT id FROM joueur WHERE alias = 'JiciTroy450'),
	TO_DATE('2018-11-02 20:02', 'YYYY-MM-DD HH24:MI'),5700);
	
INSERT INTO activite_avatar(id_activite, id_avatar, duree) 
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JiciTroy450') AND activite.date_heure_debut = TO_DATE('2018-11-02 20:02', 'YYYY-MM-DD HH24:MI')),
        (SELECT id FROM avatar WHERE nom = 'ElPredatore'),5700);
	
INSERT INTO activite_monde(id_activite, id_monde, duree) 
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JiciTroy450') AND activite.date_heure_debut = TO_DATE('2018-11-02 20:02', 'YYYY-MM-DD HH24:MI')),
        (SELECT id FROM monde WHERE sigle = 'LEIWIS'),1680);
        
INSERT INTO activite_monde(id_activite, id_monde, duree) 
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JiciTroy450') AND activite.date_heure_debut = TO_DATE('2018-11-02 20:02', 'YYYY-MM-DD HH24:MI')),
        (SELECT id FROM monde WHERE sigle = 'ENDRYA'),4020);
        
-- Obtention d'une caract�ristique        
INSERT INTO carac_avatar(id_avatar, id_carac_monde, date_obtention, niveau) 
	VALUES ((SELECT id FROM avatar WHERE nom = 'ElPredatore'),(SELECT id FROM carac_monde WHERE sigle = 'FLI'),
	TO_DATE('2018-11-02 20:02', 'YYYY-MM-DD HH24:MI'), 3);
    
-- Achat d'un item
INSERT INTO achat(id_joueur, date_paiement, id_type_achat, id_mode_paiement, id_item_rare, montant, date_debut, date_fin) 
	VALUES ((SELECT id FROM joueur WHERE alias = 'JiciTroy450'),
	TO_DATE('2018-11-02 20:18', 'YYYY-MM-DD HH24:MI'), 2, 2, (SELECT id FROM items_rare WHERE sigle = 'ILHC'), 2.99, NULL, NULL);

-- Ajout de l'item dans la table items_obtenus
INSERT INTO items_obtenus(id_avatar, id_items_rare, date_heure_obtention, quantite)
    VALUES ((SELECT id FROM avatar WHERE nom = 'ElPredatore'),(SELECT id FROM items_rare WHERE sigle = 'ILHC'), 
        TO_DATE('2018-11-02 20:18', 'YYYY-MM-DD HH24:MI'),1);
        
-- Achat d'un item
INSERT INTO achat(id_joueur, date_paiement, id_type_achat, id_mode_paiement, id_item_rare, montant, date_debut, date_fin) 
	VALUES ((SELECT id FROM joueur WHERE alias = 'JiciTroy450'),
	TO_DATE('2018-11-02 20:20', 'YYYY-MM-DD HH24:MI'), 2, 2, (SELECT id FROM items_rare WHERE sigle = 'IAAL'), 3.99, NULL, NULL);

-- Ajout de l'item dans la table items_obtenus
INSERT INTO items_obtenus(id_avatar, id_items_rare, date_heure_obtention, quantite)
    VALUES ((SELECT id FROM avatar WHERE nom = 'ElPredatore'),(SELECT id FROM items_rare WHERE sigle = 'IAAL'), 
        TO_DATE('2018-11-02 20:20', 'YYYY-MM-DD HH24:MI'),1);
        
-- Obtention d'items
INSERT INTO items_obtenus(id_avatar, id_items_rare, date_heure_obtention, quantite)
    VALUES ((SELECT id FROM avatar WHERE nom = 'ElPredatore'),(SELECT id FROM items_rare WHERE sigle = 'IEBC'), 
        TO_DATE('2018-11-02 20:44', 'YYYY-MM-DD HH24:MI'),1);
        
INSERT INTO items_obtenus(id_avatar, id_items_rare, date_heure_obtention, quantite)
    VALUES ((SELECT id FROM avatar WHERE nom = 'ElPredatore'),(SELECT id FROM items_rare WHERE sigle = 'IECC'), 
        TO_DATE('2018-11-02 20:52', 'YYYY-MM-DD HH24:MI'),1);    
    
    
-- 3e ACTIVIT� ***************************************************************************************************** 
INSERT INTO activite(id_joueur, date_heure_debut, duree) 
	VALUES ((SELECT id FROM joueur WHERE alias = 'JiciTroy450'),
	TO_DATE('2018-11-03 15:12', 'YYYY-MM-DD HH24:MI'),10440);
	
INSERT INTO activite_avatar(id_activite, id_avatar, duree) 
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JiciTroy450') AND activite.date_heure_debut = TO_DATE('2018-11-03 15:12', 'YYYY-MM-DD HH24:MI')),
        (SELECT id FROM avatar WHERE nom = 'ElPredatore'),10440);
	
INSERT INTO activite_monde(id_activite, id_monde, duree) 
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JiciTroy450') AND activite.date_heure_debut = TO_DATE('2018-11-03 15:12', 'YYYY-MM-DD HH24:MI')),
        (SELECT id FROM monde WHERE sigle = 'HROPLS'),8640);
        
INSERT INTO activite_monde(id_activite, id_monde, duree) 
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'JiciTroy450') AND activite.date_heure_debut = TO_DATE('2018-11-03 15:12', 'YYYY-MM-DD HH24:MI')),
        (SELECT id FROM monde WHERE sigle = 'AVACYN'),1800);
        
-- Obtention d'une caract�ristique        
INSERT INTO carac_avatar(id_avatar, id_carac_monde, date_obtention, niveau) 
	VALUES ((SELECT id FROM avatar WHERE nom = 'ElPredatore'),(SELECT id FROM carac_monde WHERE sigle = 'FHF'),
	TO_DATE('2018-11-03 17:52', 'YYYY-MM-DD HH24:MI'), 12);    
    
-- Achat d'un item
INSERT INTO achat(id_joueur, date_paiement, id_type_achat, id_mode_paiement, id_item_rare, montant, date_debut, date_fin) 
	VALUES ((SELECT id FROM joueur WHERE alias = 'JiciTroy450'),
	TO_DATE('2018-11-03 15:17', 'YYYY-MM-DD HH24:MI'), 2, 2, (SELECT id FROM items_rare WHERE sigle = 'IHMA'), 4.99, NULL, NULL);

-- Ajout de l'item dans la table items_obtenus
INSERT INTO items_obtenus(id_avatar, id_items_rare, date_heure_obtention, quantite)
    VALUES ((SELECT id FROM avatar WHERE nom = 'ElPredatore'),(SELECT id FROM items_rare WHERE sigle = 'IHMA'), 
        TO_DATE('2018-11-03 15:17', 'YYYY-MM-DD HH24:MI'),1);
        
-- Achat d'un item
INSERT INTO achat(id_joueur, date_paiement, id_type_achat, id_mode_paiement, id_item_rare, montant, date_debut, date_fin) 
	VALUES ((SELECT id FROM joueur WHERE alias = 'JiciTroy450'),
	TO_DATE('2018-11-03 15:19', 'YYYY-MM-DD HH24:MI'), 2, 2, (SELECT id FROM items_rare WHERE sigle = 'IABL'), 2.99, NULL, NULL);

-- Ajout de l'item dans la table items_obtenus
INSERT INTO items_obtenus(id_avatar, id_items_rare, date_heure_obtention, quantite)
    VALUES ((SELECT id FROM avatar WHERE nom = 'ElPredatore'),(SELECT id FROM items_rare WHERE sigle = 'IABL'), 
        TO_DATE('2018-11-03 15:19', 'YYYY-MM-DD HH24:MI'),1);
        
-- Obtention d'items
INSERT INTO items_obtenus(id_avatar, id_items_rare, date_heure_obtention, quantite)
    VALUES ((SELECT id FROM avatar WHERE nom = 'ElPredatore'),(SELECT id FROM items_rare WHERE sigle = 'IHQR'), 
        TO_DATE('2018-11-03 15:38', 'YYYY-MM-DD HH24:MI'),1);
        
INSERT INTO items_obtenus(id_avatar, id_items_rare, date_heure_obtention, quantite)
    VALUES ((SELECT id FROM avatar WHERE nom = 'ElPredatore'),(SELECT id FROM items_rare WHERE sigle = 'IHSG'), 
        TO_DATE('2018-11-03 16:17', 'YYYY-MM-DD HH24:MI'),1);     
        
PROMPT  
PROMPT =======================================================
PROMPT Insertions des activit�s pour le quatri�me joueur
PROMPT =======================================================
    
-- premi�re activit� YayaZoya
INSERT INTO activite(id_joueur, date_heure_debut, duree) 
	VALUES ((SELECT id FROM joueur WHERE alias = 'YayaZoya'),
	TO_DATE('2018-11-11 20:42', 'YYYY-MM-DD HH24:MI'),
	6800);
    
    INSERT INTO activite_avatar(id_activite, id_avatar, duree) 
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'YayaZoya')),
	
	(SELECT id FROM avatar WHERE nom = 'BelAmi'),
	8516);
	
INSERT INTO activite_monde(id_activite, id_monde, duree) 
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'YayaZoya')),
	
	(SELECT id FROM monde WHERE sigle = 'LEIWIS'),
	8516);
    
-- Deuxi�me activit� du joueur YayaZoya
INSERT INTO activite(id_joueur, date_heure_debut, duree) 
	VALUES ((SELECT id FROM joueur WHERE alias = 'YayaZoya'),
	TO_DATE('2018-12-02 15:40', 'YYYY-MM-DD HH24:MI'),
	70210);
	
INSERT INTO activite_avatar(id_activite, id_avatar, duree) 
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'YayaZoya'
        AND date_heure_debut = TO_DATE('2018-12-02 15:40', 'YYYY-MM-DD HH24:MI'))),
	
	(SELECT id FROM avatar WHERE nom = 'BelAmi'),
	70210);
	
INSERT INTO activite_monde(id_activite, id_monde, duree) 
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'YayaZoya'
        AND date_heure_debut = TO_DATE('2018-12-02 15:40', 'YYYY-MM-DD HH24:MI'))),
	
	(SELECT id FROM monde WHERE sigle = 'HROPLS'),
	10200);
	
INSERT INTO activite_monde(id_activite, id_monde, duree)  
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'YayaZoya'
        AND date_heure_debut = TO_DATE('2018-12-02 15:40', 'YYYY-MM-DD HH24:MI'))),
	
	(SELECT id FROM monde WHERE sigle = 'AVACYN'),
	20400);
	
-- Troisi�me activit� du joueur YayaZoya
INSERT INTO activite(id_joueur, date_heure_debut, duree) 
	VALUES ((SELECT id FROM joueur WHERE alias = 'YayaZoya'),
	TO_DATE('2018-12-04 16:56', 'YYYY-MM-DD HH24:MI'),
	70210);
	
INSERT INTO activite_avatar(id_activite, id_avatar, duree) 
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'YayaZoya'
        AND date_heure_debut = TO_DATE('2018-12-04 16:56', 'YYYY-MM-DD HH24:MI'))),
	
	(SELECT id FROM avatar WHERE nom = 'BelAmi'),
	70210);
	
INSERT INTO activite_monde(id_activite, id_monde, duree) 
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'YayaZoya'
        AND date_heure_debut = TO_DATE('2018-12-04 16:56', 'YYYY-MM-DD HH24:MI'))),
	
	(SELECT id FROM monde WHERE sigle = 'LEIWIS'),
	10200);
	
INSERT INTO activite_monde(id_activite, id_monde, duree) 
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'YayaZoya'
        AND date_heure_debut = TO_DATE('2018-12-04 16:56', 'YYYY-MM-DD HH24:MI'))),
	
	(SELECT id FROM monde WHERE sigle = 'HROPLS'),
	20400);
    
--achat d'items 	
INSERT INTO achat(id_joueur, date_paiement, id_type_achat, id_mode_paiement, id_item_rare, montant, date_debut, date_fin) 
	VALUES ((SELECT id FROM joueur WHERE alias = 'YayaZoya'),
	TO_DATE('2018-11-11 23:11', 'YYYY-MM-DD HH24:MI'), 2, 2, (SELECT id FROM items_rare WHERE sigle = 'ILPE'), 5.99, Null, Null);
    
INSERT INTO achat(id_joueur, date_paiement, id_type_achat, id_mode_paiement, id_item_rare, montant, date_debut, date_fin) 
	VALUES ((SELECT id FROM joueur WHERE alias = 'YayaZoya'),
	TO_DATE('2018-12-09 16:18', 'YYYY-MM-DD HH24:MI'), 2, 1, (SELECT id FROM items_rare WHERE sigle = 'IHMI'), 6.99, Null, Null);
    
    
--insertion des items dans la table items obtenus
INSERT INTO items_obtenus(id_avatar, id_items_rare, date_heure_obtention, quantite)
    VALUES ((SELECT id FROM avatar WHERE nom = 'BelAmi'),(SELECT id FROM items_rare WHERE sigle = 'ILPE'), 
        TO_DATE('2018-11-11 23:11', 'YYYY-MM-DD HH24:MI'),1);
        
INSERT INTO items_obtenus(id_avatar, id_items_rare, date_heure_obtention, quantite)
    VALUES ((SELECT id FROM avatar WHERE nom = 'BelAmi'),(SELECT id FROM items_rare WHERE sigle = 'IHMI'), 
        TO_DATE('2018-12-09 16:18', 'YYYY-MM-DD HH24:MI'),1);  
        
--insertion des CARACT�RISTIQUES       
INSERT INTO carac_avatar(id_avatar, id_carac_monde, date_obtention, niveau) 
	VALUES ((SELECT id FROM avatar WHERE nom = 'BelAmi'),(SELECT id FROM carac_monde WHERE sigle = 'FEG'),
	TO_DATE('2018-12-11 14:50', 'YYYY-MM-DD HH24:MI'), 45
	);
INSERT INTO carac_avatar(id_avatar, id_carac_monde, date_obtention, niveau) 
	VALUES ((SELECT id FROM avatar WHERE nom = 'BelAmi'),(SELECT id FROM carac_monde WHERE sigle = 'FIN'),
	TO_DATE('2018-12-13 7:50', 'YYYY-MM-DD HH24:MI'), 62
	);
    
PROMPT   
PROMPT ===========================================================================
PROMPT Insertions des activit�s/achats/caract�risques/items Joueuse : 'Shaazaah'
PROMPT ===========================================================================

-- Achat : temps 2 mois jusqu'au 5 d�c 2018
INSERT INTO achat(id_joueur, date_paiement, id_type_achat, id_mode_paiement, id_item_rare, montant, date_debut, date_fin) 
	VALUES ((SELECT id FROM joueur WHERE alias = 'KalamityKat'),
    TO_DATE('2018-10-05 17:04', 'YYYY-MM-DD HH24:MI'),1,2,NULL,59.99,TO_DATE('2018-10-05', 'YYYY-MM-DD'),TO_DATE('2018-12-05', 'YYYY-MM-DD'));
	
--Achat : items rare pour chaque monde
INSERT INTO achat(id_joueur, date_paiement, id_type_achat, id_mode_paiement, id_item_rare, montant, date_debut, date_fin) 
	VALUES ((SELECT id FROM joueur WHERE alias = 'KalamityKat'),
	TO_DATE('2018-10-06 17:34', 'YYYY-MM-DD HH24:MI'), 2, 1, (SELECT id FROM items_rare WHERE sigle = 'IHQR'),6.99, NULL, NULL);
	
INSERT INTO achat(id_joueur, date_paiement, id_type_achat, id_mode_paiement, id_item_rare, montant, date_debut, date_fin) 
	VALUES ((SELECT id FROM joueur WHERE alias = 'KalamityKat'),
	TO_DATE('2018-10-06 17:38', 'YYYY-MM-DD HH24:MI'), 2, 1, (SELECT id FROM items_rare WHERE sigle = 'IARB'),4.99, NULL, NULL);	
INSERT INTO achat(id_joueur, date_paiement, id_type_achat, id_mode_paiement, id_item_rare, montant, date_debut, date_fin) 

	VALUES ((SELECT id FROM joueur WHERE alias = 'KalamityKat'),
	TO_DATE('2018-10-06 17:45', 'YYYY-MM-DD HH24:MI'), 2, 1, (SELECT id FROM items_rare WHERE sigle = 'ILMO'),3.99, NULL, NULL);	
INSERT INTO achat(id_joueur, date_paiement, id_type_achat, id_mode_paiement, id_item_rare, montant, date_debut, date_fin) 

	VALUES ((SELECT id FROM joueur WHERE alias = 'KalamityKat'),
	TO_DATE('2018-10-06 17:50', 'YYYY-MM-DD HH24:MI'), 2, 1, (SELECT id FROM items_rare WHERE sigle = 'IEBM'),2.99, NULL, NULL);	

-- Ajout des items dans la table items_obtenus
INSERT INTO items_obtenus(id_avatar, id_items_rare, date_heure_obtention, quantite)
    VALUES ((SELECT id FROM avatar WHERE nom = 'Shaazaah'),(SELECT id FROM items_rare WHERE sigle = 'IHQR'), 
        TO_DATE('2018-10-06 17:34', 'YYYY-MM-DD HH24:MI'),1);
		
INSERT INTO items_obtenus(id_avatar, id_items_rare, date_heure_obtention, quantite)
    VALUES ((SELECT id FROM avatar WHERE nom = 'Shaazaah'),(SELECT id FROM items_rare WHERE sigle = 'IARB'), 
        TO_DATE('2018-10-06 17:38', 'YYYY-MM-DD HH24:MI'),1);
		
INSERT INTO items_obtenus(id_avatar, id_items_rare, date_heure_obtention, quantite)
    VALUES ((SELECT id FROM avatar WHERE nom = 'Shaazaah'),(SELECT id FROM items_rare WHERE sigle = 'IHQR'), 
        TO_DATE('2018-10-06 17:45', 'YYYY-MM-DD HH24:MI'),1);
		
INSERT INTO items_obtenus(id_avatar, id_items_rare, date_heure_obtention, quantite)
    VALUES ((SELECT id FROM avatar WHERE nom = 'Shaazaah'),(SELECT id FROM items_rare WHERE sigle = 'IEBM'), 
        TO_DATE('2018-10-06 17:50', 'YYYY-MM-DD HH24:MI'),1);

--1re activite

-- Log Joueur:
INSERT INTO activite(id_joueur, date_heure_debut, duree) 
	VALUES ((SELECT id FROM joueur WHERE alias = 'KalamityKat'),
	TO_DATE('2018-10-10 17:30', 'YYYY-MM-DD HH24:MI'),8500);
    
--Log avatar
INSERT INTO activite_avatar(id_activite, id_avatar, duree)
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'KalamityKat')
		AND date_heure_debut = TO_DATE('2018-10-10 17:30', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM avatar WHERE nom = 'Shaazaah'),
	8500);
    
--log monde
INSERT INTO activite_monde(id_activite, id_monde, duree) 
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'KalamityKat')
        AND date_heure_debut = TO_DATE('2018-10-10 17:30', 'YYYY-MM-DD HH24:MI')),
        (SELECT id FROM monde WHERE sigle = 'LEIWIS'),8500);    
        
--obtention d'item    
INSERT INTO items_obtenus(id_avatar, id_items_rare, date_heure_obtention, quantite)
    VALUES ((SELECT id FROM avatar WHERE nom = 'Shaazaah'),(SELECT id FROM items_rare WHERE sigle = 'ILMP'), 
        TO_DATE('2018-10-10 18:45', 'YYYY-MM-DD HH24:MI'),1); 
    
    
    
--2eme activite
-- Log Joueur:
INSERT INTO activite(id_joueur, date_heure_debut, duree) 
	VALUES ((SELECT id FROM joueur WHERE alias = 'KalamityKat'),
	TO_DATE('2018-10-16 16:20', 'YYYY-MM-DD HH24:MI'),9600);
    
--Log avatar
INSERT INTO activite_avatar(id_activite, id_avatar, duree)
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'KalamityKat')
		AND date_heure_debut = TO_DATE('2018-10-16 16:20', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM avatar WHERE nom = 'Shaazaah'),
	9600);
--Log monde    
INSERT INTO activite_monde(id_activite, id_monde, duree) 
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'KalamityKat')
        AND date_heure_debut =TO_DATE('2018-10-16 16:20', 'YYYY-MM-DD HH24:MI')),
        (SELECT id FROM monde WHERE sigle = 'HROPLS'),9600);  
--item obtenu        
INSERT INTO items_obtenus(id_avatar, id_items_rare, date_heure_obtention, quantite)
    VALUES ((SELECT id FROM avatar WHERE nom = 'Shaazaah'),(SELECT id FROM items_rare WHERE sigle = 'IHMN'), 
        TO_DATE('2018-10-16 17:25', 'YYYY-MM-DD HH24:MI'),1);
        
        
--3eme activite
--Log joueur
INSERT INTO activite(id_joueur, date_heure_debut, duree) 
	VALUES ((SELECT id FROM joueur WHERE alias = 'KalamityKat'),
	TO_DATE('2018-11-23 7:30', 'YYYY-MM-DD HH24:MI'),34000);
--Log avatar
INSERT INTO activite_avatar(id_activite, id_avatar, duree)
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'KalamityKat')
		AND date_heure_debut = TO_DATE('2018-11-23 7:30', 'YYYY-MM-DD HH24:MI')),
	
	(SELECT id FROM avatar WHERE nom = 'Shaazaah'),
	34230);
 --Log monde   
INSERT INTO activite_monde(id_activite, id_monde, duree) 
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'KalamityKat')
        AND date_heure_debut = TO_DATE('2018-11-23 7:30', 'YYYY-MM-DD HH24:MI')),
        (SELECT id FROM monde WHERE sigle = 'ENDRYA'),11230); 
--caract�ristique obtenue      
INSERT INTO carac_avatar(id_avatar, id_carac_monde, date_obtention, niveau) 
	VALUES ((SELECT id FROM avatar WHERE nom = 'Shaazaah'),(SELECT id FROM carac_monde WHERE sigle = 'FEL'),
	TO_DATE('2018-11-23 9:47', 'YYYY-MM-DD HH24:MI'), 60); 
--Log monde    
INSERT INTO activite_monde(id_activite, id_monde, duree)  
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'KalamityKat')
        AND date_heure_debut = TO_DATE('2018-11-23 7:30', 'YYYY-MM-DD HH24:MI')),
        (SELECT id FROM monde WHERE sigle = 'AVACYN'),10540);  
--item obtenu        
INSERT INTO items_obtenus(id_avatar, id_items_rare, date_heure_obtention, quantite)
    VALUES ((SELECT id FROM avatar WHERE nom = 'Shaazaah'),(SELECT id FROM items_rare WHERE sigle = 'IAAL'), 
        TO_DATE('2018-11-23 8:35', 'YYYY-MM-DD HH24:MI'),1);
        
--log monde
INSERT INTO activite_monde(id_activite, id_monde, duree) 
	VALUES ((SELECT id FROM activite
	WHERE id_joueur = (
		SELECT id FROM joueur
		WHERE alias = 'KalamityKat')
        AND date_heure_debut = TO_DATE('2018-11-23 7:30', 'YYYY-MM-DD HH24:MI')),
        (SELECT id FROM monde WHERE sigle = 'LEIWIS'),12460); 
        
--caract�ristique obtenue       
INSERT INTO carac_avatar(id_avatar, id_carac_monde, date_obtention, niveau) 
	VALUES ((SELECT id FROM avatar WHERE nom = 'Shaazaah'),(SELECT id FROM carac_monde WHERE sigle = 'FLC'),
	TO_DATE('2018-11-23 16:12', 'YYYY-MM-DD HH24:MI'), 83);   
 	

