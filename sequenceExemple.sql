DROP SEQUENCE seq_joueur_id

CREATE SEQUENCE seq_joueur_id
	INCREMENT BY 1
	START WITH 1;
	
INSERT INTO joueur 
	VALUES (seq_joueur_id.NEXTVAL, 
			'RoboRidley', 
			'danick.massicotte@gmail.com', 
			'yhnmju678', 
			'M', 
			sysdate, 
			TO_DATE('1987-11-14', 'YYYY-MM-DD'));