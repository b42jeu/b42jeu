--select 'drop table ', table_name, 'cascade constraints;' from user_tables;
drop table 	TYPE_ACHAT	cascade constraints;
drop table 	TEMPS_JEU_ACHETE	cascade constraints;
drop table 	PHRASES_PERSO	cascade constraints;
drop table 	MONDE	cascade constraints;
drop table 	MODE_PAIEMENT	cascade constraints;
drop table 	LIAISON_MONDE_ITEMS_RARE	cascade constraints;
drop table 	LIAISON_MONDE_CARAC	cascade constraints;
drop table 	LIAISON_ITEMS_OBTENUS_AVATAR	cascade constraints;
drop table 	LIAISON_ACTIVITE_MONDE	cascade constraints;
drop table 	LIAISON_ACTIVITE_AVATAR	cascade constraints;
drop table 	JOUEUR	cascade constraints;
drop table 	ITEMS_RARE	cascade constraints;
drop table 	ITEMS_OBTENUS	cascade constraints;
drop table 	COULEUR_PREFEREE	cascade constraints;
drop table 	CARAC_AVATAR	cascade constraints;
drop table 	CARAC_MONDE	cascade constraints;
drop table 	AVATAR	cascade constraints;
drop table 	ACTIVITE	cascade constraints;
drop table 	ACHAT	cascade constraints;


select 'drop table ', table_name, 'cascade constraints;' from user_tables;