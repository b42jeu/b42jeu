
-- NOM DU PROJET: B42 Projet 1
-- NOM DU FICHIER: MasterCreateTable
-- DATE CR�ATION: 2018-09-21
-- DESCRIPTION: Cr�ation de toutes les tables avec leur Foreign keys
-- AUTEURS: Jean-Charles Bertrand, Yasmine Kaddouri, Lynda Lavoie, Danick Massicotte

/*********************************************************
* NOTES POUR NOUS: A FAIRE/MODIFICATIONS, etc
*********  lundi 24 septembre ********
*  Ajouter des sequences autres des Generate On Null
  
*********************************************************/

-- SET ECHO OFF

PROMPT  
PROMPT =======================================================
PROMPT Destruction des tables
PROMPT =======================================================

drop table 	TYPE_ACHAT	cascade constraints;
drop table 	TEMPS_ACHETE	cascade constraints;
drop table 	PHRASES_PERSO	cascade constraints;
drop table 	MONDE	cascade constraints;
drop table 	MODE_PAIEMENT	cascade constraints;
drop table 	ACTIVITE_MONDE	cascade constraints;
drop table 	ACTIVITE_AVATAR	cascade constraints;
drop table 	JOUEUR	cascade constraints;
drop table 	ITEMS_RARE	cascade constraints;
drop table 	ITEMS_OBTENUS	cascade constraints;
drop table 	COULEUR_PREFEREE	cascade constraints;
drop table 	CARAC_AVATAR	cascade constraints;
drop table 	CARAC_MONDE	cascade constraints;
drop table 	AVATAR	cascade constraints;
drop table 	ACTIVITE	cascade constraints; 
drop table 	ACHAT	cascade constraints;

PROMPT  
PROMPT =======================================================
PROMPT Destruction des s�quences
PROMPT =======================================================

DROP SEQUENCE seq_joueur_id;
DROP SEQUENCE seq_avatar_id;

PROMPT  
PROMPT =======================================================
PROMPT Cr�action des s�quences
PROMPT =======================================================

CREATE SEQUENCE seq_joueur_id
	INCREMENT BY 1
	START WITH 1;
	
CREATE SEQUENCE seq_avatar_id
	INCREMENT BY 1
	START WITH 1;

PROMPT  
PROMPT =======================================================
PROMPT Cr�ation des tables
PROMPT =======================================================

--	id						NUMBER GENERATED BY DEFAULT ON NULL AS IDENTITY,  --sans parentheses(10)

CREATE TABLE achat (
	id						NUMBER(10)		GENERATED BY DEFAULT ON NULL AS IDENTITY,
	id_joueur				NUMBER(10)		NOT NULL,
	date_paiement 			DATE			NOT NULL,
	id_type_achat			NUMBER(2)		NOT NULL,
	id_mode_paiement		NUMBER(2)		NOT NULL,
	id_item_rare		    NUMBER(10),
    id_temps_achete         NUMBER(10),
	montant					NUMBER(6,2)		NOT NULL,
    CONSTRAINT pk_achat_id PRIMARY KEY (id),
    CONSTRAINT cc_achat_montant CHECK (montant >= 0.01 AND montant <= 2500),
	CONSTRAINT cc_achat_typeAchat CHECK (id_type_achat >= 1 AND id_type_achat <=2),          --2 types d'achats seulement
	CONSTRAINT cc_achat_modePaiement CHECK (id_mode_paiement >=1 AND id_mode_paiement <=3)  --3 types de mode de paiement seulement
);

CREATE TABLE activite (
	id					NUMBER(10)  GENERATED BY DEFAULT ON NULL AS IDENTITY,
	id_joueur			NUMBER(10)	NOT NULL,
	date_heure_debut	DATE		NOT NULL,
	duree				NUMBER(6)  	NOT NULL,

	CONSTRAINT pk_activite_id		PRIMARY KEY(id),
    CONSTRAINT cc_activite_duree	CHECK (duree > 0)
);
CREATE TABLE activite_avatar (
	id_activite			NUMBER(10)		NOT NULL,
	id_avatar			NUMBER(10)		NOT NULL,
	duree		        NUMBER(6),
	
	CONSTRAINT pk_ActAvatar			PRIMARY KEY(id_activite, id_avatar),
	CONSTRAINT cc_activite_duree_avatar 	CHECK (duree > 0)
);
CREATE TABLE activite_monde (
	id_activite			NUMBER(10)		NOT NULL,
	id_monde			NUMBER(10)		NOT NULL,
	duree		        NUMBER(6),
	
	CONSTRAINT pk_ActMonde			PRIMARY KEY(id_activite, id_monde),
	CONSTRAINT cc_activite_duree_monde 	CHECK (duree > 0)
);

CREATE TABLE avatar(
    id                          NUMBER,                 
    id_joueur                   NUMBER(10)      NOT NULL,
    nom                         VARCHAR2(32)    NOT NULL, 
    date_creation               DATE            NOT NULL,
    qte_mox                     NUMBER(10),
    CONSTRAINT pk_avatar_id         PRIMARY KEY (id),
    CONSTRAINT cc_avatar_qteMoX 
        CHECK (qte_mox BETWEEN -1000000000 AND 10000000000)
);
    
	
CREATE TABLE carac_avatar(
    id_avatar               NUMBER(2) ,
    id_carac_monde          NUMBER(2),
    date_obtention          DATE NOT NULL,
    niveau                  NUMBER(3) DEFAULT 0,
    
    CONSTRAINT cc_niveau        CHECK(niveau BETWEEN 0 AND 999),
    CONSTRAINT pk_carac_avatar PRIMARY KEY (id_avatar, id_carac_monde)
);    


CREATE TABLE carac_monde (
    id                      NUMBER              GENERATED BY DEFAULT ON NULL AS IDENTITY,
    nom         	        VARCHAR2(32)        NOT NULL,
    sigle   		        VARCHAR2(3)         NOT NULL,
	energie_acquisition     NUMBER(5,2)         NOT NULL,
    energie_utilisation     NUMBER(7,3)         NOT NULL,        
    description             VARCHAR2(512),
    id_monde                NUMBER(10)          NOT NULL,
    
    CONSTRAINT pk_carac_monde                   PRIMARY KEY (id),
    CONSTRAINT uc_carac_monde_nom               UNIQUE(nom),     
    CONSTRAINT cc_carac_monde_sigle 
        CHECK (sigle LIKE 'F__'),
    CONSTRAINT uc_carac_monde_sigle             UNIQUE(sigle),
    CONSTRAINT cc_caracMonde_EnerAcqui CHECK (energie_acquisition BETWEEN 25 AND 250),
    CONSTRAINT cc_caracMonde_EnerUtil CHECK (energie_utilisation BETWEEN -1000 AND 1000)
);

CREATE TABLE couleur_preferee (
	id				NUMBER(10) 		GENERATED BY DEFAULT ON NULL AS IDENTITY,
	id_avatar		NUMBER(10) 		NOT NULL,
	red  			NUMBER(3)		NOT NULL,
	green			NUMBER(3)		NOT NULL,
	blue			NUMBER(3)		NOT NULL,
	
	CONSTRAINT pk_couleur_id	PRIMARY KEY(id),
    CONSTRAINT cc_couleur_red	CHECK (red BETWEEN 0 AND 255),
	CONSTRAINT cc_couleur_green	CHECK (green BETWEEN 0 AND 255),
	CONSTRAINT cc_couleur_blue	CHECK (blue BETWEEN 0 AND 255)
);

CREATE TABLE items_obtenus (
	id  NUMBER(10) GENERATED BY DEFAULT ON NULL AS IDENTITY,
	id_avatar NUMBER(10),
	id_items_rare NUMBER(10),
    date_heure_obtention DATE NOT NULL,
    quantite NUMBER(3),
	
	CONSTRAINT pk_items_obtenus_id PRIMARY KEY (id),
    CONSTRAINT cc_qty CHECK (quantite BETWEEN 1 AND 100)
    

);

CREATE TABLE items_rare (
	id NUMBER GENERATED BY DEFAULT ON NULL AS IDENTITY,
    id_monde NUMBER(10),
	nom 	VARCHAR2(32) NOT NULL ,
	sigle	VARCHAR2(4) NOT NULL, 
    probabilite NUMBER(10,9) NOT NULL,
    cout	NUMBER(13,2) NOT NULL,
    description VARCHAR2(512) NOT NULL,
    
	CONSTRAINT pk_items_rare_id PRIMARY KEY (id),
    CONSTRAINT uc_items_rare_nom UNIQUE(nom),
    CONSTRAINT cc_sigle CHECK (sigle LIKE 'I___'),
    CONSTRAINT cc_probabilite CHECK (probabilite BETWEEN 0 AND 1), -- a tester 
    CONSTRAINT cc_cout CHECK (cout BETWEEN 0 AND 100000000) -- a tester  
);


CREATE TABLE joueur (
	id		                        NUMBER, 
    alias                           VARCHAR(32)         NOT NULL,
    courriel	                    VARCHAR2(128)       NOT NULL,
    mot_de_passe                    VARCHAR2(32)        NOT NULL,
    genre                           CHAR(1)             NOT NULL,  
    date_inscription                DATE, 
    date_de_naissance               DATE,
    
    CONSTRAINT pk_joueur_id             PRIMARY KEY (id),
    CONSTRAINT cc_joueur_genre       
    CHECK (UPPER(genre) IN ('F','M','X')),
    /*
        CHECK (genre = 'F' OR 
                genre = 'f' OR 
                genre = 'M' OR 
                genre = 'm' OR 
                genre = 'X' OR 
                genre = 'x'),
                */
	CONSTRAINT uc_joueur_alias          UNIQUE(alias),
    CONSTRAINT uc_joueur_courriel       UNIQUE(courriel),
    CONSTRAINT cc_joueur_dateInscrip    CHECK (date_inscription >= TO_DATE('01/01/2018', 'DD/MM/YYYY')),
    CONSTRAINT cc_joueur_dateNaiss      CHECK (date_de_naissance >= TO_DATE('01/01/1900', 'DD/MM/YYYY'))
);

CREATE TABLE mode_paiement (
	id			NUMBER(2)			GENERATED BY DEFAULT ON NULL AS IDENTITY,
	nom			VARCHAR2(32)		NOT NULL,
    CONSTRAINT pk_modePaiement_id PRIMARY KEY (id)
);

CREATE TABLE monde(
    id                          NUMBER(10)          GENERATED BY DEFAULT ON NULL AS IDENTITY,                 
    nom                         VARCHAR2(16)        NOT NULL, 
    sigle                       VARCHAR2(6)         NOT NULL,
    description                 VARCHAR2(2048),
   
    CONSTRAINT pk_monde_id      PRIMARY KEY(id),
    CONSTRAINT uc_monde_nom     UNIQUE(nom),
    CONSTRAINT uc_monde_sigle   UNIQUE(sigle),
    CONSTRAINT cc_monde_sigle 
        CHECK (sigle LIKE '______')
);

CREATE TABLE phrases_perso (
	id			NUMBER(10)		GENERATED BY DEFAULT ON NULL AS IDENTITY,
	phrases		VARCHAR2(64),
	id_avatar	NUMBER(7)		NOT NULL,
	
	CONSTRAINT pk_phrases_id	PRIMARY KEY (id)
);

CREATE TABLE temps_achete (
	id				NUMBER(10)		GENERATED BY DEFAULT ON NULL AS IDENTITY,
	date_debut		DATE			NOT NULL,
	date_fin		DATE,
	
	CONSTRAINT pk_tempsAchete_id 			    PRIMARY KEY(id),
    CONSTRAINT cc_tempsAchete_dateFin           CHECK (date_fin > date_debut)
);

CREATE TABLE type_achat (
	id			NUMBER(2)		GENERATED BY DEFAULT ON NULL AS IDENTITY,
	nom			VARCHAR2(32)	NOT NULL,
    CONSTRAINT pk_typeAchat_id PRIMARY KEY (id)
);

PROMPT  
PROMPT =======================================================
PROMPT Cr�ation des foreigns Keys
PROMPT =======================================================

--OK
ALTER TABLE achat   
    ADD CONSTRAINT fk_achat_id_mode_paiement
        FOREIGN KEY (id_mode_paiement)
        REFERENCES mode_paiement(id);

-- OK 
ALTER TABLE achat   
ADD CONSTRAINT fk_achat_id_type_achat
        FOREIGN KEY (id_type_achat)
        REFERENCES type_achat(id);
 
--OK
ALTER TABLE achat   
    ADD CONSTRAINT fk_temps_achete
        FOREIGN KEY (id_temps_achete)
        REFERENCES temps_achete(id);

-- OK 
ALTER TABLE achat   
ADD CONSTRAINT fk_item_rare_achete
        FOREIGN KEY (id_item_rare)
        REFERENCES items_rare(id);
        
ALTER TABLE achat   
ADD CONSTRAINT fk_achat_joueur
        FOREIGN KEY (id_joueur)
        REFERENCES joueur(id);

ALTER TABLE activite   
    ADD CONSTRAINT fk_activite_joueur
        FOREIGN KEY (id_joueur) 
        REFERENCES joueur(id);
        
ALTER TABLE activite_avatar         
    ADD CONSTRAINT fk_actAv_avatar
        FOREIGN KEY (id_avatar)
        REFERENCES avatar(id);
        
ALTER TABLE activite_avatar         
    ADD CONSTRAINT fk_actAv_activite
        FOREIGN KEY (id_activite)
        REFERENCES activite(id);
       
ALTER TABLE activite_monde         
    ADD CONSTRAINT fk_actMo_monde
        FOREIGN KEY (id_monde)
        REFERENCES monde(id);
        
ALTER TABLE activite_monde         
    ADD CONSTRAINT fk_actMo_activite
        FOREIGN KEY (id_activite)
        REFERENCES activite(id);
        
ALTER TABLE avatar
    ADD CONSTRAINT fk_avatar_id_joueur
        FOREIGN KEY (id_joueur) 
        REFERENCES joueur(id) ;
        
ALTER TABLE carac_avatar         
    ADD CONSTRAINT fk_carac_avatar_id_avatar
        FOREIGN KEY (id_avatar)
        REFERENCES avatar(id);
--OK      
ALTER TABLE carac_avatar          
    ADD CONSTRAINT fk_carac_avatar_carac_monde 
        FOREIGN KEY (id_carac_monde)
        REFERENCES carac_monde(id);
        
ALTER TABLE carac_monde
    ADD CONSTRAINT fk_carac_monde_id_monde
        FOREIGN KEY (id_monde)
        REFERENCES monde(id);
        
ALTER TABLE couleur_preferee   
    ADD CONSTRAINT fk_couleur_preferee_id_avatar
        FOREIGN KEY (id_avatar)
        REFERENCES avatar(id);
        
ALTER TABLE items_obtenus   
    ADD CONSTRAINT fk_items_obtenus_id_avatar
        FOREIGN KEY (id_avatar)
        REFERENCES avatar(id);
--OK
ALTER TABLE items_rare       
    ADD CONSTRAINT fk_items_rare_monde
        FOREIGN KEY (id_monde)
        REFERENCES monde(id);

        
ALTER TABLE phrases_perso   
    ADD CONSTRAINT fk_phrases_perso_id_avatar
        FOREIGN KEY (id_avatar)
        REFERENCES avatar(id);
        
        
PROMPT  
PROMPT =======================================================
PROMPT Insertions modes de paiement
PROMPT =======================================================

INSERT INTO mode_paiement(nom) 
    VALUES('carte de cr�dit');

INSERT INTO mode_paiement(nom) 
    VALUES('paypal');

INSERT INTO mode_paiement(nom) 
    VALUES('interact');

PROMPT  
PROMPT =======================================================
PROMPT Insertions types achats
PROMPT =======================================================

INSERT INTO type_achat(nom) 
    VALUES('temps de jeu');

INSERT INTO type_achat(nom) 
    VALUES('item rare');
    
    
